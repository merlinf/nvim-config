utils = require('utils') -- Expose global utils accessible via utils.func

require('settings') -- General settings
require('mappings') -- Key mappings
require('plugins') -- Load plugins

 -- vim.cmd('colorscheme catppuccin')

