-- Utility functions

local M = {}

-- Wrapper for nvim_set_keymap
function M.map(mode, from, to, opts)
    local options = { noremap = true }

    if opts then
        options = vim.tbl_extend("force", options, opts)
    end

    vim.api.nvim_set_keymap(mode, from, to, options)
end

return M
