-- Mappings go here
local wk = require('which-key')

utils.map('i', 'jk', '<Esc>') -- Exit insert mode on jk

-- Leader mappings (through which-key)
vim.g.mapleader = ' ' -- Set <Leader> to <Space>

wk.register({
    ['/'] = { '<cmd>lua require("telescope.builtin").live_grep()<CR>', 'Search' },
    ['\\'] = { ':noh<CR>', 'Clear highlights' },
    f = { -- File manipulation
        name = 'file',
        f = { '<cmd>lua require("telescope.builtin").find_files()<CR>', 'Find file' },
        s = { ':w<CR>', 'Save file' }
    },
    h = { -- Things to help me out
        name = 'help',
        m = { '<cmd>lua require("telescope.builtin").man_pages()<CR>', 'Find man page' }
    },
    i = { -- Insert stuff
        name = 'insert',
        e = {
            '<cmd>lua require("telescope.builtin").symbols({prompt_title="Emoji",sources={"emoji"}})<CR>',
            'Emoji'
        },
        g = {
            '<cmd>lua require("telescope.builtin").symbols({prompt_title="Gitmoji",sources={"gitmoji"}})<CR>',
            'Gitmoji'
        },
        k = {
            '<cmd>lua require("telescope.builtin").symbols({prompt_title="Kaomoji (╯°□°）╯︵ ┻━┻",sources={"kaomoji"}})<CR>',
            'Kaomoji'
        },
        m = {
            '<cmd>lua require("telescope.builtin").symbols({prompt_title="Math Symbols",sources={"math"}})<CR>',
            'Math'
        },
        n = { 'o<Esc>k', 'New line bottom' },
        N = { 'O<Esc>j', 'New line top' }
    },
    m = { -- Miscellanious
        name = 'misc.',
        s = { '<cmd>if exists("g:syntax_on") | syntax off | else | syntax enable | endif<CR>', 'Syntax highlighting (toggle)' },
        s = {
            function()
                if (vim.g.syntax_on) then
                    vim.cmd('syntax off')
                    vim.cmd('TSDisable highlight')
                else
                    vim.cmd('syntax on')
                    vim.cmd('TSEnable highlight')
                end
            end,
            'Syntax highlighting'
        },
        t = { -- Themes
            name = 'themes',
            a = { '<cmd>colorscheme alabaster<CR>', 'Alabaster' },
            c = { '<cmd>colorscheme catppuccin<CR>', 'Catppuccin' }
        }
    },
    q = { -- Quitting
        name = 'quit',
        q = { ':q<CR>', 'Quit Nvim' },
        Q = { ':q!<CR>', 'Quit Nvim without saving' }
    },
    u = {
        name = 'utils',
        p = {
            name = 'packer',
            c = { '<cmd>lua require("packer").clean()<CR>', 'Clean unused plugins' },
            s = { '<cmd>lua require("packer").sync()<CR>', 'Sync plugins' }
        },
        m = { '<cmd>Glow<CR>', 'Markdown preview' }
    }
}, { prefix = '<Leader>' })

