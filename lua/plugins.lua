local fn = vim.fn

local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  packer_bootstrap = fn.system({
    'git',
    'clone',
    '--depth',
    '1',
    'https://github.com/wbthomason/packer.nvim',
    install_path
  })
end

vim.cmd 'packadd packer.nvim'

return require('packer').startup(function(use)
    -- Plugins go here
    use 'wbthomason/packer.nvim' -- Plugin Manager, you're using it right now :)

    use { -- Catppuccin theme
        'catppuccin/nvim',
        as = 'catppuccin',
        config = function() require('plugin_configs.catppuccin') end,
        -- TODO: remove this once the feiline integration is fixed
        commit = '8a67df6da476cba68ecf26a519a5279686edbd2e'
    }

    use { -- Pretty statusline
        'feline-nvim/feline.nvim',
        requires = 'kyazdani42/nvim-web-devicons',
        config = function() require('plugin_configs.feline') end
    }

    use { -- Popups displaying possible keybindings
        'folke/which-key.nvim',
        config = function() require('plugin_configs.which-key') end
    }

    use { -- Advanced Syntax Highlighting
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate',
        config = function() require('plugin_configs.treesitter') end
    }

    use { -- Fuzzy finder
        'nvim-telescope/telescope.nvim',
        requires = {
            'nvim-lua/plenary.nvim',
            {
                'nvim-telescope/telescope-fzf-native.nvim',
                run = 'make'
            },
            'nvim-telescope/telescope-symbols.nvim'
        }
    }

    use 'tpope/vim-abolish' -- string manipulation stuff
                            -- (author even doesn't know what exactly it is)

    use { -- Markdown preview (glow needs to be installed as well)
        'ellisonleao/glow.nvim',
        branch = 'main'
    }

    use { -- Smooth scrolling
        'karb94/neoscroll.nvim',
        config = function() require('plugin_configs.neoscroll') end
    }

    -- Automatically set up configuration after cloning packer.nvim
    if packer_bootstrap then
        require('packer').sync()
    end
end)

