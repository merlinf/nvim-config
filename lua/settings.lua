local opt = vim.opt

local indent = 4 -- Indentation width

vim.api.nvim_command('language en_US') -- Set language

vim.cmd('colorscheme catppuccin') -- Set colorscheme

-- Indentation
opt.autoindent = true -- Copies indenting from previous line
opt.smartindent = false -- Automatically indent next line further
                        -- only use when file type based indentation isn't satisfactory
opt.shiftwidth = indent -- Spaces for each step of (auto)indent
opt.shiftround = true -- Round to 'shiftwidth' for "<<" and ">>"
opt.expandtab = true -- Expand <Tab> to spaces in insert mode
opt.softtabstop = indent -- If non-zero, number of spaces to insert for <Tab>
opt.smarttab = true -- <Tab> in front of line inserts 'shiftwidth' spaces
                    -- <BS> will delete 'shiftwidth' spaces

-- Searching
opt.ignorecase = true -- Search case insensitive
opt.smartcase = true -- Search case sensitive when query contains capital letters

-- QOL
opt.number = true -- Show line numbers
opt.cursorline = true -- Highlight current line
opt.mouse = 'a' -- Enable mouse for all modes
opt.gdefault = true -- Use global by default on replaces (:s/foo/bar/ -> :s/foo/bar/g)

opt.clipboard = 'unnamed,unnamedplus' -- Use system clipboard
opt.syntax = 'on' -- Enables syntax highlighting
opt.timeoutlen = 500 -- Time in milliseconds to wait for a mapped sequence to complete
