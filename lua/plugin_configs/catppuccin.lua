require('catppuccin').setup({
    integrations = {
        treesitter = true,
        telescope = true,
        which_key = true
    }
})
